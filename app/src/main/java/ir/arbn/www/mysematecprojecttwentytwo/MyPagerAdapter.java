package ir.arbn.www.mysematecprojecttwentytwo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by A.R.B.N on 2/28/2018.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return FragmentA.getIns();
        else if (position == 1)
            return FragmentB.getIns();
        else if (position == 2)
            return FragmentC.getIns();
        else if (position == 3)
            return FragmentD.getIns();
        else if (position == 4)
            return FragmentE.getIns();
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Fragment A";
        else if (position == 1)
            return "Fragment B";
        else if (position == 2)
            return "Fragment C";
        else if (position == 3)
            return "Fragment D";
        else if (position == 4)
            return "Fragment E";
        return super.getPageTitle(position);
    }
}
