package ir.arbn.www.mysematecprojecttwentytwo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
findViewById(R.id.switchBTN).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent next=new Intent(HomeActivity.this, SecondActivity.class);
        startActivity(next);
    }
});    }
}
