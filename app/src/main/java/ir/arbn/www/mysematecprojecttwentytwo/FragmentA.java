package ir.arbn.www.mysematecprojecttwentytwo;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by A.R.B.N on 2/28/2018.
 */

public class FragmentA extends Fragment {
    String videoURL = "https://as2.cdn.asset.aparat.com/aparat-video/6413fd7649b76f42ec4dfa250973d2839654776-144p__95663.mp4";
    private static FragmentA frag;

    public static FragmentA getIns() {
        if (frag == null)
            frag = new FragmentA();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a, container, false);
        VideoView myVideo = view.findViewById(R.id.myVideo);
        myVideo.setMediaController(new MediaController(getActivity()));
        myVideo.setVideoURI(Uri.parse(videoURL));
        myVideo.start();
        return view;
    }
}
