package ir.arbn.www.mysematecprojecttwentytwo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by A.R.B.N on 2/28/2018.
 */

public class FragmentC extends Fragment {
    private static FragmentC frag;

    public static FragmentC getIns() {
        if (frag == null)
            frag = new FragmentC();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c, container, false);
        return view;
    }
}
